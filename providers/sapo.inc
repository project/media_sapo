<?php

/**
 * @file
 *   This is an include file used by emfield.module.
 */
define('EMVIDEO_SAPO_MAIN_URL', 'http://videos.sapo.pt/');
define('EMVIDEO_SAPO_DATA_VERSION', 1.1);

function emvideo_sapo_info() {
  $name = t('Sapo.pt');
  $features = array(
    array(t('Autoplay'), t('No'), ''),
    array(t('RSS Attachment'), t('No'), ''),
    array(t('Thumbnails'), t('Yes'), ''),
    array(t('Duration'), t('No'), ''),
  );
  return array(
    'provider' => 'sapo',
    'name' => $name,
    'url' => EMVIDEO_SAPO_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from <a href="@provider" target="_blank">Sapo.pt</a>.', array('@provider' => EMVIDEO_SAPO_MAIN_URL)),
    'supported_features' => $features,
    'weight' => 9,
  );
}

function emvideo_sapo_settings() {
  $form = array();
  return $form;
}

/**
 *  Implement hook emvideo_PROVIDER_data_version().
 */
function emvideo_sapo_data_version() {
  return EMVIDEO_SAPO_DATA_VERSION;
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the 8tracks provider.
 */
function emvideo_sapo_data($field, $item) {
  $data = array();

  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
  $data['emvideo_data_version'] = $data['emvideo_sapo_version'] = EMVIDEO_SAPO_DATA_VERSION;

  return $data;
}

/**
 *  Implements emvideo_PROVIDER_extract().
 */
function emvideo_sapo_extract($embed = '') {
  // <embed src="http://rd3.videos.sapo.pt/play?file=http://rd3.videos.sapo.pt/a6MpUJuynM9wpz1eYZrA/mov/1" type="application/x-shockwave-flash" width="400" height="322"></embed>
  // http://videos.sapo.pt/a6MpUJuynM9wpz1eYZrA
  return array(
    '@videos\.sapo\.pt/(\w+)/mov@i',
    '@videos\.sapo\.pt/(\w+)@i',
  );
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the audio code.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_sapo_embedded_link($video_code) {
  return 'http://videos.sapo.pt/' . $video_code;
}

function theme_emvideo_sapo_flash($embed, $width, $height, $autoplay = FALSE, $field = NULL, $item = NULL) {
  if ($embed) {
    return '<embed src="http://rd3.videos.sapo.pt/play?file=http://rd3.videos.sapo.pt/'. $embed .'/mov/1" width="'. $width .'" height="'. $height .'" quality="high" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';
  }
}

function emvideo_sapo_thumbnail($field, $item, $formatter, $node, $width, $height) {
  if (!empty($item['value'])) {
    return "http://videos.sapo.pt/$item[value]/pic";
  }
  else {
    return;
  }
}

function emvideo_sapo_video($code, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_sapo_flash', $code, $width, $height, $autoplay, $field, $item);
  return $output;
}

function emvideo_sapo_preview($code, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_sapo_flash', $code, $width, $height, $autoplay, $field, $item);
  return $output;
}

/**
 * Implementation of hook_emfield_subtheme.
 */
function emvideo_sapo_emfield_subtheme() {
  return array(
    'emvideo_sapo_flash' => array(
      'arguments' => array('embed' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
      'file' => 'providers/sapo.inc',
      'path' => drupal_get_path('module', 'media_sapo'),
    )
  );
}
